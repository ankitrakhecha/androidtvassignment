package com.ankit.androidtvsample.dependecyinjection;

import com.ankit.androidtvsample.AndroidTvSampleApplication;
import com.ankit.androidtvsample.views.MainActivityPresenter;
import com.ankit.androidtvsample.views.PlayerActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules={DataModule.class,PlayerModule.class})
public interface DataComponent {
    void inject(AndroidTvSampleApplication dagger2Sample);
    void inject(MainActivityPresenter mainActivityPresenter);
    void inject(PlayerActivity playerActivity);

}
