package com.ankit.androidtvsample.Utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import com.ankit.androidtvsample.constants.ApiConstatnts;
import com.squareup.picasso.Picasso;

public class MyBindingUtils {
    @BindingAdapter("app:setImage")
    public static void setImageUrl(ImageView imageView,String imageUrl)
    {
        Picasso.get()
                .load(ApiConstatnts.IMAGE_BASE_URL+ imageUrl)
                .into(imageView);
    }
}
