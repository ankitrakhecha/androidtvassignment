package com.ankit.androidtvsample.network;


import com.ankit.androidtvsample.models.ApiModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class Repository {

    private ApiService apiService;

    @Inject
    public Repository(ApiService apiService){
        this.apiService = apiService;
    }

    public Single<ApiModel> getNetworkItems(){
        return apiService.getData();
    }
}
