package com.ankit.androidtvsample.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.Observable;
import android.os.Parcel;
import android.os.Parcelable;

import com.ankit.androidtvsample.BR;

public class PlayerModel extends BaseObservable implements Parcelable {
    private String videoUrl;
    private boolean isLoading=false;

    public PlayerModel(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    protected PlayerModel(Parcel in) {
        videoUrl = in.readString();
        isLoading = in.readByte() != 0;
    }

    public static final Creator<PlayerModel> CREATOR = new Creator<PlayerModel>() {
        @Override
        public PlayerModel createFromParcel(Parcel in) {
            return new PlayerModel(in);
        }

        @Override
        public PlayerModel[] newArray(int size) {
            return new PlayerModel[size];
        }
    };
    @Bindable
    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
        notifyPropertyChanged(BR.videoUrl);

    }

    @Bindable
    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
        notifyPropertyChanged(BR.loading);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(videoUrl);
        parcel.writeByte((byte) (isLoading ? 1 : 0));
    }

}
