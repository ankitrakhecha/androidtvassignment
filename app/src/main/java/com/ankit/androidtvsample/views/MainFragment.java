/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.ankit.androidtvsample.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v17.leanback.app.BrowseSupportFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;

import com.ankit.androidtvsample.leanbackpresenters.CardPresenter;
import com.ankit.androidtvsample.models.ApiModel;
import com.ankit.androidtvsample.models.PlayerModel;

import java.util.List;

import static android.support.v17.leanback.widget.FocusHighlight.ZOOM_FACTOR_NONE;
import static com.ankit.androidtvsample.constants.AppConstants.PLAYER_PARCELABLE_MODEL;

public class MainFragment extends BrowseSupportFragment implements OnItemViewClickedListener, OnItemViewSelectedListener,MainActivityContract.View {
    private static final String TAG = "MainFragment";
    private MainActivityPresenter mainActivityPresenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHeadersState(BrowseSupportFragment.HEADERS_DISABLED);
        setHeadersTransitionOnBackEnabled(true);
        mainActivityPresenter = new MainActivityPresenter(this);
        mainActivityPresenter.onShowData();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupUIElements();
    }

    private void setupUIElements() {
        setupEventListeners();
    }

    private void setupEventListeners() {
        setOnItemViewClickedListener(this);
        setOnItemViewSelectedListener(this);
    }


    private void loadData(ApiModel apiModel) {
        List<ApiModel.CategoriesBean> list = apiModel.getCategories();

        ArrayObjectAdapter rowsAdapter = new ArrayObjectAdapter(new ListRowPresenter(ZOOM_FACTOR_NONE));
        CardPresenter cardPresenter = new CardPresenter();

        int i;
        for (i = 0; i < list.size(); i++) {
            ApiModel.CategoriesBean categoriesBean = list.get(i);
            HeaderItem header = new HeaderItem(i, categoriesBean.getName());
            ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(cardPresenter);
            listRowAdapter.addAll(0,categoriesBean.getVideos());
            rowsAdapter.add(new ListRow(header, listRowAdapter));
        }
        setAdapter(rowsAdapter);
    }


    @Override
    public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {
        if (item instanceof ApiModel.CategoriesBean.VideosBean) {
            ApiModel.CategoriesBean.VideosBean videosBean= (ApiModel.CategoriesBean.VideosBean) item;
            Intent playerActivityIntent = new Intent(getActivity(),PlayerActivity.class);

            PlayerModel playerModel = new PlayerModel(videosBean.getSources().get(0));
            playerActivityIntent.putExtra(PLAYER_PARCELABLE_MODEL,playerModel);
            startActivity(playerActivityIntent);
        }
    }

    @Override
    public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showData(ApiModel apiModel) {
        loadData(apiModel);
    }

    @Override
    public void onStop() {
        super.onStop();
        mainActivityPresenter.clear();
    }
}
