package com.ankit.androidtvsample.views;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.media.AudioManager;
import android.os.Bundle;

import com.ankit.androidtvsample.AndroidTvSampleApplication;
import com.ankit.androidtvsample.R;
import com.ankit.androidtvsample.Utils.PlayerUtils;
import com.ankit.androidtvsample.databinding.ActivityPlayerBinding;
import com.ankit.androidtvsample.models.PlayerModel;
import com.ankit.androidtvsample.network.PlayerRepository;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

import javax.inject.Inject;

import static com.ankit.androidtvsample.constants.AppConstants.PLAYER_PARCELABLE_MODEL;

public class PlayerActivity extends Activity implements Player.EventListener, AudioManager.OnAudioFocusChangeListener {
    ExoPlayer player;
    private ActivityPlayerBinding playerBinding;
    private AudioManager audioManager;
    private PlayerModel playerModel;

    @Inject
    PlayerRepository playerRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playerBinding = DataBindingUtil.setContentView(this, R.layout.activity_player);

         AndroidTvSampleApplication.getDagger2App().getDataComponent().inject(this);
         audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
         playerModel =getIntent().getParcelableExtra(PLAYER_PARCELABLE_MODEL);

         initializePlayer();
         playerBinding.setPlayerModel(playerModel);
    }

    private void initializePlayer() {
        if (player == null) {
            player = playerRepository.getExoPlayer();
            player.addListener(this);
            playerBinding.simpleExoPlayer.setPlayer(player);
            player.prepare(PlayerUtils.createMediaSource(playerModel.getVideoUrl()));

            int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            if(result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                player.setPlayWhenReady(true);
        }
    }


    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releasePlayer();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case Player.STATE_BUFFERING:
                playerModel.setLoading(true);
                break;
            case Player.STATE_ENDED:
                finish();
                break;
            case Player.STATE_IDLE:
                playerModel.setLoading(false);
                break;
            case Player.STATE_READY:
                playerModel.setLoading(false);
                break;
            default:
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                player.setPlayWhenReady(true);
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                player.stop();
                break;
        }
    }
}
